# Portfolio

Portfolio website : https://nsobczak.github.io/portfolio/

## Colors

- general page background color: #173D54
- darker blue : #173D48
- dark blue : #155A6A
- blue : #3BBDBD
- light blue : #a3f5eb
- light yellow : #E2F0D6
- orange : #F76F07
